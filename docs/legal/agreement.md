# **Grassroots Economics Commons Agreement (en)**

## **Preamble**

We **Grassroots Economics Foundation** _(herein called the Founding Member)_ hereby ratify and incorporate the[ Grassroots Economics Commons License v1](https://docs.grassecon.org/legal/license/) in the following Economic Commons (herein called the Grassroots Economics Commons) as its Guardian, General Member and Platform Service Provider.


## **Platform:**

The Platform of the Grassroots Economics Commons (herein called Sarafu Network) will utilize the Founding Member as the Platform Service Provider:

1. **Ledger Services:**
    1. The Founding Member will deploy their own and other Member Instruments and register them on the ledger known as: [Kitabu Blockchain](/software/kitabu)
2. **Public Access:** transaction data and user information will be generally available to Researchers and Members and (subject to a one month waiting period) individual balances and trade history will be provided to individual Members and Mediators upon request. See [data protection policy](https://docs.grassecon.org/legal/data_policy/).
3. **Terms and Conditions:** When using the Platform the Applicant agrees to the [Terms and Conditions](https://grassecon.org/pages/terms-and-conditions.html) of Grassroots Economics Foundation.

## **Instruments**

1. Voucher: All further mention of Vouchers in this agreement will refer to the Founding Member’s unique Voucher with the following properties and constraints: 

    1. **Voucher Name**: e.g. Sarafu (Minimum 3 and Maximum 32 characters)
    2. **Voucher Symbol**: e.g. SRF (Minimum 3 and Maximum 6 characters used for display on the Platform)
    3. **Voucher Supply**: Inital SRF Vouchers will be minted for preexisting Sarafu users and additional SRF will minted and allocated to each additional Member when joining Sarafu Network.
    5. **Unit of Account and Denomination**: Grassroots Economics Foundation service minutes
    5. **Acceptance rate**: 100 SRF will be redeemable as payment for a single Voucher creation service or equivalent value of services from Grassroots Economics Foundation.
    6. **Offering and Value**: Grassroots Economics Foundation Services can be obtained through SRF Vouchers, including but not limited to:
        1. Training and Support related to Economic Commons and Clearing Unions, Exchange Platforms, Instruments and related Member provided services
        2. Platform and Instrument reporting, creation and maintenance
        3. Software integration and customization
        4. Arbitration of disputes related to Instruments and Platform usage
    7. **Expiration Rate**: The Vouchers expire each minute with cumulative rate of 2% per month.
    8. **Community Account: Issuance**: Created vouchers will be placed into the following account: 0xBBb4a93c8dCd82465B73A143f00FeD4AF7492a27
    9. **Community Account: Expiration**: All expired Vouchers will be distributed to the following account specified here: 0xBBb4a93c8dCd82465B73A143f00FeD4AF7492a27
2. Token: Non-Confidential Member data utilizing any Platform registered Instrument is made available to Members by mutual agreement in the form of Tokens (herein called an Impact Tokens) which hold endorsements and reports on Instrument usage. See our [data protection policy](https://docs.grassecon.org/legal/data_policy/). 
3. Swap: Value supplied by Members in money or in-kind exchanged for Vouchers or Impact Tokens, will be used to maintain the Sarafu Network infrastructure and to support Members as specified by mutual agreement. 

## **Addendum:**

1. **Good Faith:** All Members and holders of Instruments on Sarafu Network enter into this Agreement in good faith and hold harmless Members of the Grassroots Economics Commons.
2. **Entirety:** this application represents consent to the entirety of a mutual agreement between Grassroots Economics Commons Members to the expressed common purpose and no other.
3. **Maintenance and Contacts:** This Agreement is maintained by Grassroots Economics Foundation and can be reached at tel: +254757628885 mail: PO Box, 1659-80108 Kilifi Kenya email: info@grassecon.org
